﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Speech.Synthesis;
using System.Diagnostics;
using System.Media;

namespace SpeechSynthesizerSample
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        SpeechSynthesizer ss = new SpeechSynthesizer();
        SoundPlayer player = new SoundPlayer();

        public MainWindow()
        {
            InitializeComponent();

            ss.SpeakCompleted +=new EventHandler<SpeakCompletedEventArgs>( ss_SpeakCompleted );
        }

        void ss_SpeakCompleted( object sender, SpeakCompletedEventArgs e )
        {
            try {
                player.Stream.Position = 0;
                player.Play();
            }
            catch ( Exception ex ) {
                MessageBox.Show( ex.Message );
            }
        }

        private void button1_Click( object sender, RoutedEventArgs e )
        {
            try {
                player.Stream = new System.IO.MemoryStream();
                ss.SetOutputToWaveStream( player.Stream );
                ss.SpeakAsync( textBox1.Text );
            }
            catch ( Exception ex ) {
                MessageBox.Show( ex.Message );
            }
        }
    }
}
