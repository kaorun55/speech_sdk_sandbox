﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenNI;
using Win32;
using Microsoft.Speech.Recognition;
using SpeechRecognizer;
using System.IO;
using Microsoft.Speech.AudioFormat;
using System.Runtime.InteropServices;

namespace XtionSpeechRecognize
{
    class OpenNIAudioStream : Stream
    {
        AudioGenerator audio;
        long position = 0;

        public OpenNIAudioStream( AudioGenerator audio )
        {
            this.audio = audio;
        }

        public override IAsyncResult BeginRead( byte[] buffer, int offset, int count, AsyncCallback callback, object state )
        {
            return base.BeginRead( buffer, offset, count, callback, state );
        }
        public override int ReadByte()
        {
            return base.ReadByte();
        }
        public override int Read( byte[] buffer, int offset, int count )
        {
            if ( !audio.IsDataNew ) {
                audio.WaitAndUpdateData();
                position = 0;
            }

            Marshal.Copy( (IntPtr)((int)audio.AudioBufferPtr + position), buffer, offset, count );
            position += count;
            return count;
        }

        public override void Write( byte[] buffer, int offset, int count )
        {
            throw new NotImplementedException();
        }

        public override void SetLength( long value )
        {
            throw new NotImplementedException();
        }

        public override long Seek( long offset, SeekOrigin origin )
        {
            return 0;
        }

        public override void Flush()
        {
            throw new NotImplementedException();
        }

        public override long Position
        {
            get
            {
                return position;
            }

            set
            {
                position = value;
            }
        }

        public override long Length
        {
            get
            {
                if ( !audio.IsDataNew ) {
                    audio.WaitAndUpdateData();
                }

                return audio.DataSize;
            }
        }

        public override bool CanSeek
        {
            get
            {
                return false;
            }
        }

        public override bool CanWrite
        {
            get
            {
                return false;
            }
        }

        public override bool CanRead
        {
            get
            {
                return true;
            }
        }
    }

    class Program
    {
        static void Main( string[] args )
        {
            try {
                Context context = new Context();
                AudioGenerator audio = new AudioGenerator( context );
                context.StartGeneratingAll();

                StreamingWavePlayer wavePlayer = new StreamingWavePlayer( audio.WaveOutputMode.SampleRate,
                    audio.WaveOutputMode.BitsPerSample, audio.WaveOutputMode.Channels, 100 );

                var colors = new Choices();
                colors.Add( "red" );
                colors.Add( "green" );
                colors.Add( "blue" );
                colors.Add( "end" );
                colors.Add( "赤" );
                colors.Add( "みどり" );
                colors.Add( "あお" );
                colors.Add( "終わり" );

                Recognizer r = new Recognizer( "ja-JP", colors );
                r.SpeechRecognized += SreSpeechRecognized;
                r.SpeechHypothesized += SreSpeechHypothesized;
                r.SpeechRecognitionRejected += SreSpeechRecognitionRejected;
                Console.WriteLine( "Using: {0}", r.Name );

                Console.WriteLine( "Mic start. Press any key to stop." );

                OpenNIAudioStream s = new OpenNIAudioStream( audio );

                r.SetInputToAudioStream( s,
                        new SpeechAudioFormatInfo(
                            EncodingFormat.Pcm, 16000, 16, 1,
                            32000, 2, null ) );
                r.RecognizeAsync( RecognizeMode.Multiple );
                Console.ReadLine();
                Console.WriteLine( "Stopping recognizer ..." );
                r.RecognizeAsyncStop();
            }
            catch ( Exception ex ) {
                Console.WriteLine( ex.Message );
            }
        }

        static void SreSpeechRecognitionRejected( object sender, SpeechRecognitionRejectedEventArgs e )
        {
            Console.WriteLine( "\nSpeech Rejected" );
        }

        static void SreSpeechHypothesized( object sender, SpeechHypothesizedEventArgs e )
        {
            Console.Write( "\rSpeech Hypothesized: \t{0}", e.Result.Text );
        }

        static void SreSpeechRecognized( object sender, SpeechRecognizedEventArgs e )
        {
            //This first release of the Kinect language pack doesn't have a reliable confidence model, so 
            //we don't use e.Result.Confidence here.
            Console.WriteLine( "\nSpeech Recognized: \t{0}", e.Result.Text );
        }
    }
}
